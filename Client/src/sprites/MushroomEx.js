import Phaser from 'phaser'
import { Mushroom } from './Mushroom.js'

class MushroomEx extends Phaser.GameObjects.Container {
    constructor(scene, x, y) {
        super(scene, x, y);
        this.scene = scene;
        this.x = x;
        this.y = y;
        scene.add.existing(this);
        scene.physics.world.enable(this);
        this.text=scene.add.text(-40, 0, "");
        this.mushroom = new Mushroom(this.scene, 0, 50);
        this.add([this.mushroom, this.text]);
        this.up = false;
        this.down = false;
        this.left = false;
        this.right = false;
    }

    setText(str){
        this.text.setText(str);
    }

    goUp() {
        this.up = true;
    }
    goDown() {
        this.down = true;
    }
    goLeft() {
        this.left = true;
    }
    goRight() {
        this.right = true;
    }
    idle() {
        this.up = this.down = this.left = this.right = false;
    }

    update() {
        this.setText(document.getElementById("message").value);
        this.body.setVelocity(0);
        if (this.left) {
            this.body.setVelocityX(-50);
        } else if (this.right) {
            this.body.setVelocityX(50);
        } else if (this.up) {
            this.body.setVelocityY(-30);
        } else if (this.down) {
            this.body.setVelocityY(30);
        }
    }
}
export {MushroomEx};