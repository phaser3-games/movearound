import Phaser from 'phaser'

class Mushroom extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'mushroom');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.up=false;
    this.down=false;
    this.left=false;
    this.right=false;
  }

  goUp(){
    this.up=true;
  }
  goDown(){
    this.down=true;
  }
  goLeft(){
    this.left=true;
  }
  goRight(){
    this.right=true;
  }
  idle(){
    this.up=this.down=this.left=this.right=false;
  }

  update () {
    this.setVelocity(0);
    if(this.left){
      this.setVelocityX(-50);
    }else if(this.right){
      this.setVelocityX(50);
    }else if(this.up){
      this.setVelocityY(-30);
    }else if(this.down){
      this.setVelocityY(30);
    }
  }
}
export {Mushroom};