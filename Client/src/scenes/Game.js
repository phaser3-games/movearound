import Phaser from 'phaser'
import {Mushroom}  from '../sprites/Mushroom.js'
import {MushroomEx}  from '../sprites/MushroomEx.js'
import io from 'socket.io-client';

class Game extends Phaser.Scene {
  constructor () {
    super({ key: 'Game' })
  }

  create () {
    let self=this;
    this.id=""+new Date().getTime();
    this.mushroom = new MushroomEx(this, 0, 0);
    this.cursors = this.input.keyboard.createCursorKeys();
    this.socket=io("http://localhost:8000");
    this.socket.emit('join', {
      "id": self.id
    });
    this.others={};
    this.modified=false;
    
    this.socket.on("move", function(o){
      let player=self.others[o.id];
      if(player){
        console.log("exists!"+o.id);
        player.x=o.x;
        player.y=o.y;
      }else{
        let other=new MushroomEx(self, o.x, o.y);
        self.others[o.id]=other;
      }
    });
  }

  update () {
    let self=this;
    this.mushroom.idle();
    if(this.cursors.left.isDown){
      this.modified=true;
      this.mushroom.goLeft();
    }else if(this.cursors.right.isDown){
      this.modified=true;
      this.mushroom.goRight();
    }else if(this.cursors.up.isDown){
      this.modified=true;
      this.mushroom.goUp();
    }else if(this.cursors.down.isDown){
      this.modified=true;
      this.mushroom.goDown();
    }
    this.mushroom.update();
    if(this.modified){
      this.modified=false;
      this.socket.emit('move', {
        "id": self.id,
        "x": this.mushroom.x,
        "y": this.mushroom.y
      });
    }
    
  }
}

export {Game};