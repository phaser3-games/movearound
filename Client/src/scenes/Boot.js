import Phaser from 'phaser'
import mushroom from 'images/mushroom2.png'

class Boot extends Phaser.Scene {
  constructor () {
    super({ key: 'Boot' })
  }

  preload () {
    this.load.image('mushroom', mushroom)
  }

  create () {
    this.scene.start('Game')
  }
}

export {Boot};