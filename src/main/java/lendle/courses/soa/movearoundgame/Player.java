/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.movearoundgame;

import com.corundumstudio.socketio.SocketIOClient;

/**
 *
 * @author lendle
 */
public class Player {
    private int x=0;
    private int y=0;
    private SocketIOClient socketClient=null;
    private String id=null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    

    public SocketIOClient getSocketClient() {
        return socketClient;
    }

    public void setSocketClient(SocketIOClient socketClient) {
        this.socketClient = socketClient;
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    
}
