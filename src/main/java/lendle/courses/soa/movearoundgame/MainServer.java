/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.movearoundgame;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import java.util.HashMap;
import java.util.Map;
import lendle.courses.soa.movearoundgame.events.JoinEvent;
import lendle.courses.soa.movearoundgame.events.MoveEvent;

/**
 *
 * @author lendle
 */
public class MainServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        Map<String, Player> players=new HashMap<>();
        Configuration config = new Configuration();
        config.setPort(8000);
        SocketIOServer server = new SocketIOServer(config);
        server.addConnectListener(new ConnectListener() {
            @Override
            public void onConnect(SocketIOClient sioc) {
                System.out.println("connected: "+sioc.getSessionId());
            }
        });
        
        server.addEventListener("join", JoinEvent.class, new DataListener<JoinEvent>() {
            @Override
            public void onData(SocketIOClient sioc, JoinEvent t, AckRequest ar) throws Exception {
                Player player=new Player();
                player.setId(t.getId());
                players.put(player.getId(), player);
            }
        });
        
        server.addEventListener("move", MoveEvent.class, new DataListener<MoveEvent>() {
            @Override
            public void onData(SocketIOClient sioc, MoveEvent t, AckRequest ar) throws Exception {
                Player player=players.get(t.getId());
                if(player!=null){
                    player.setX(t.getX());
                    player.setY(t.getY());
                }
                server.getBroadcastOperations().sendEvent("move", t);
            }
        });
        
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                server.stop();
            }
        });
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (Throwable e) {
                server.stop();
                break;
            }
        }
    }
    
}
